# Sirus Authentication Service

Authentication service for all sirus apps

## Requirement

---

These application is required to run the project, older version might still work but unoptimized

- Node.js: `^8.9.1`
- NPM: `^5.5.1`
- PM2: `^2.9.1`

## Installation

---

- After cloning the project into your local workspace, use `npm install` to install all dependencies

#### Using Console

- Run the API on your local workspace by `node auth.js`
- Go to `localhost:5000` or [Click Here](http://localhost:5000) to check if the server work, Also check the console wether the signature is successfully generated

#### Using PM2

- Run the API on your local workspace by `npm start`
- Go to `localhost:5000` or [Click Here](http://localhost:5000) to check if the server work, Also check the console wether the signature is successfully generated

#### Using Executable

- Build the API using `npm run binary` (for linux) or `npm run binary-dev` (for windows)
- Run the executable on `./dist` directory
- Go to `localhost:5000` or [Click Here](http://localhost:5000) to check if the server work, Also check the console wether the signature is successfully generated

## Route List

---

### **Authentication**

Base URL: `localhost:5000/authentication`

- **Get Token**  
  Route: BASE URL
  Method: `POST`
  Body Structure:  
   1. `email`: User Email 2. `password`: User Password

- **Add User**  
  Route: Base URL + `/addUser`
  Method: `POST`  
  Body Structure: \*\*\*\* 1. `userid`: User ID 2. `nodok`: User Doctor Number 3. `nik`: User Employment Number 4. `email`: User Email 5. `password`: User Password 6. `role`: User Role

- **Forgot Password**
  Route: Base URL + `/forget/:email`  
  Method: `GET`  
  `email`: User Email

- **Reset Password**
  Route: Base URL + `/reset/:email`  
  Method: `GET`  
  `email`: User Email

- **Set New Password**
  Route: Base URL + `/resetprocess`  
  Method: `POST`  
  Body Structure:  
   1. `email`: User Email 2. `password`: User Password

### **User Management API (WIP)**

Base URL: `localhost:5000/api`

- **Get User List**  
  Route: Base URL + `/users`  
  Method: `GET`

- **Get User By ID**  
  Route: Base URL + `/users/:id`  
  Method: `GET`  
  `id`: User ID

- **Delete User By ID**  
  Route: Base URL + `/delete/:id`  
  Method: `DELETE`  
  `id`: User ID

**Note**: All Transaction on API Route need Token on Header `x-access-token`
