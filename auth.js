var express = require("express"); //create express
var app = express(); //initialization
var bodyParser = require("body-parser"); //get parameter from our POST request
var config = require("./config"); //get our config file
var User = require("./app/model/user"); //get out mongoose model
var router = express.Router();
var jwt = require("jsonwebtoken");
var cors = require("cors");
var port = config.port; //set port
var engines = require("consolidate");
var path = require("path");
var ejs = require("ejs");
//enable cors
app.use(cors());
//secret variable
app.set("superSecret", config.secret);
app.set("view engine", "ejs");

//use body parser and we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", function(req, res) {
  res.json({ message: 'Hello, Welcome to SIRUS Rawat Jalan Auth API' });
});

//dummy insert
require("./app/routes/dummy-insert.js")(app, User);

//api and authentication
require("./app/routes/auth-routes.js")(app, router, jwt, User, ejs);
require("./app/routes/api-routes.js")(app, router, jwt, User);

app.on("error", err => {
  console.log("[API-AUTH] Error :" + err);
});

//start the server
app.listen(port);
console.log("Magic Happens at http://localhost:" + port);
