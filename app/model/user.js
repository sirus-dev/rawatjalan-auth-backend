var db = require("../config/dbconnection"); //reference of dbconnection.js

var User = {
  getAllUsers: function(callback) {
    return db.query("SELECT * FROM users", callback);
  },

  checkUser: function(email, password, callback) {
    return db.query(
      "SELECT * FROM users WHERE email=? AND password=SHA2(?,0)",
      [email, password],
      callback
    );
  },

  getUserByEmail: function(email, callback) {
    return db.query("SELECT * FROM users WHERE email=?", [email], callback);
  },

  getToken: function(token, callback) {
    return db.query("SELECT * FROM users WHERE token=?", [token], callback);
  },

  addUser: function(User, callback) {
    return db.query(
      "INSERT INTO users (userid,  nodok, nik, email, password, role) VALUES(?,?,?,?,SHA2(?,0),?)",
      [User.userid, User.nodok, User.nik, User.email, User.password, User.role],
      callback
    );
  },

  deleteUser: function(email, callback) {
    return db.query("DELETE FROM users WHERE userid=?", [email], callback);
  },

  updateLoginDate: function(email, callback) {
    var today = new Date();
    var DD = today.getDate();
    var MM = today.getMonth() + 1;
    var YYYY = today.getFullYear();
    var hh = today.getHours();
    var mm = today.getMinutes();
    var ss = today.getSeconds();

    if (DD < 10) {
      DD = "0" + DD;
    }

    if (MM < 10) {
      MM = "0" + MM;
    }

    today = DD + "/" + MM + "/" + YYYY + " " + hh + ":" + mm + ":" + ss;

    return db.query(
      "UPDATE users SET login_date=?, token=? WHERE email=?",
      [today, null, email],
      callback
    );
  },

  updateToken: function(email, token, callback) {
    return db.query(
      "UPDATE users SET token=? WHERE email=?",
      [token, email],
      callback
    );
  },

  setPassword: function(email, password, callback) {
    return db.query(
      "UPDATE users SET password=SHA2(?,0) WHERE email=?",
      [password, email],
      callback
    );
  }
};
module.exports = User;
