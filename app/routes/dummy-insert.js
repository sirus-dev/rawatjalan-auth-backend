var db = require('../config/dbconnection');

module.exports = function(app, User) {

app.get('/setup', function(req, res) {

  // create a sample user
  var dummyaccount = { 
    userid: '1',
    email: 'rakamaheka@gmail.com', 
    password: 'mumumumu',
    role: 'perawat' 
  };

  // save the sample user
  User.addUser(dummyaccount,function(err, result){
    if(err)
      {
      res.json(err);
      }else{
        res.json(result);
      }
  });
});

app.get('/data/:email', function(req, res){

  console.log(req.params.email);

  User.getUserByEmail(req.params.email, function(err, rows){
    if(err)
      {
      res.json(err);
      }
      else{
      res.json(rows);
      }
  });

});

}