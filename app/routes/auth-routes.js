const nodemailer = require("nodemailer");

var path = require("path");

module.exports = function(app, router, jwt, User, ejs) {
  router.post("/", function(req, res) {
    // find the user
    User.checkUser(req.body.email, req.body.password, function(err, user) {
      if (err) {
        throw err;
      }
      console.log(user);
      if (user.length > 0) {
        User.updateLoginDate(user[0].email, function(err, data) {
          if (err) {
            res.json(err);
          } else {
            getToken();
          }
        });

        function getToken() {
          User.getUserByEmail(user[0].email, function(err, row) {
            if (row[0].token == null) {
              // create a token
              var token = jwt.sign(row[0], app.get("superSecret"), {
                expiresIn: 60 * 60 * 24 // expires in 24 hours
              });

              User.updateToken(row[0].email, token, function(err, data) {
                if (err) {
                  res.json(err);
                }
              });

              res.json({
                success: true,
                message: "Enjoy your token!",
                token: token
              });
            } else {
              res.json(row);
            }
          });
        }
      } else {
        res.json({
          success: false,
          message: "Authentication failed. User not found."
        });
      }
    });
  });

  router.get("/token/:token", function(req, res) {
    User.getToken(req.params.token, function(err, users) {
      res.json(users);
    });
  });

  router.post("/addUser", function(req, res) {
    var user = {
      userid: req.body.userid,
      nodok: req.body.nodok,
      nik: req.body.nik,
      email: req.body.email,
      password: req.body.password,
      role: req.body.role
    };
    User.addUser(user, function(err, users) {
      if (err) {
        throw err;
      }
      res.json({ success: true, message: "User has been added" });
    });
  });

  router.get("/forget/:email", function(req, res) {
    User.getUserByEmail(req.params.email, function(err, users) {
      res.json(users);

      // console.log(users);
      if (users.length > 0) {
        nodemailer.createTestAccount((err, account) => {
          // create reusable transporter object using the default SMTP transport
          let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
              user: "sirusrsmary@gmail.com", // generated ethereal user
              pass: "sirusassul2017" // generated ethereal password
            }
          });

          // setup email data with unicode symbols

          ejs.renderFile(
            path.resolve(__dirname, "../views/template/forget.ejs"),
            { email: req.params.email },
            function(err, str) {
              if (err) throw err;

              if (str) {
                let mailOptions = {
                  from:
                    '"Sirus RS Mary Cileungsi 🚑" <sirusmarycileungsi@sirus.com>', // sender address
                  to: req.params.email, // list of receivers
                  subject: "Atur Ulang Kata Sandi Sirus Anda", // Subject line
                  // text: 'Hello world?', // plain text body
                  html: str // html body
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    return console.log(error);
                  }
                  console.log("Message sent: %s", info.messageId);
                  res.json({
                    success: true,
                    message: "Message sent: %s " + info.messageId
                  });
                });
              }
            }
          );
        });
      } else {
        res.json({
          success: false,
          message: "Send Email failed. User not found."
        });
      }
    });
  });

  router.get("/reset/:email", function(req, res) {
    res.render(path.resolve(__dirname, "../views/template/reset.ejs"), {
      email: req.params.email
    });
  });

  router.post("/resetprocess", function(req, res) {
    User.setPassword(req.body.email, req.body.password, function(err, users) {
      res.render(path.resolve(__dirname, "../views/template/resetresult.ejs"));
    });
  });

  // apply the routes to our application with the prefix /authentication
  app.use("/authentication", router);
};
