module.exports = function(app, router, jwt, User){
    
    // TODO: route middleware to verify a token
    
    router.use(function(req, res, next) {
    
      // check header or url parameters or post parameters for token
      var token = req.body.token || req.query.token || req.headers['x-access-token'];
    
      // decode token
      if (token) {
    
        // verifies secret and checks exp
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });    
          } else {
            // if everything is good, save to request for use in other routes
            req.decoded = decoded;    
            next();
          }
        });
    
      } else {
    
        // if there is no token
        // return an error
        return res.status(403).send({ 
            success: false, 
            message: 'No token provided.' 
        });
    
      }
    });
    
    // route to show a random message (GET http://localhost:8080/api/)
    router.get('/', function(req, res) {
      res.json({ message: 'Welcome to the coolest API on earth!' });
    });
    
    // route to return all users (GET http://localhost:8080/api/users)
    router.get('/users', function(req, res) {
      User.find({}, function(err, users) {
        res.json(users);
      });
    });   
    
    router.get('/user/:id', function(req, res) {
      User.findById({_id:req.params.id}, function(err, users) {
        res.json(users);
      });
    });   
    
    
    router.delete('/delete/:id', function(req, res){
        
        User.findByIdAndRemove({_id :req.params.id}, function(err,data){
            if(err){
                res.send(err);
            }
            res.json({ message: 'Successfully deleted' });
        });     
    });
    
    
    // apply the routes to our application with the prefix /api
    app.use('/api', router);

}