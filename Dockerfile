FROM node:11.1.0-alpine
COPY . /app
WORKDIR /app 
RUN npm install --production
EXPOSE 5000
ENTRYPOINT [ "npm", "run"]
CMD [ "start"]